//Alec Morin
//2239209
package vehicles;
public class Bicycle{

    private String manufacturer; 
    private int numberGears; 
    private double maxSpeed;

    public Bicycle(String newManufac, int newNumGears, double newMaxSpeed){
        this.manufacturer = newManufac;
        this.numberGears = newNumGears;
        this.maxSpeed = newMaxSpeed;
    }

    public String getManufactuer(){
        return this.manufacturer;
    }

    public int getNumberGears(){
        return this.numberGears;
    }

    public double getMaxSpeed(){
        return this.maxSpeed;
    }

    public String toString(){
        return "Manufacturer: "+ this.manufacturer + ", Number of Gears: "+ this.numberGears +", MaxSpeed: "+ this.maxSpeed;
    }
}