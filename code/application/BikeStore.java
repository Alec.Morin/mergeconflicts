package application;
//Alec Morin
//2239209
import vehicles.Bicycle;
public class BikeStore{
    public static void main(String[]args){
        Bicycle[] bikeGang = new Bicycle[4];
        bikeGang[0] = new Bicycle("Amazon", 4, 30);
        bikeGang[1] = new Bicycle("Spacex", 25, 121);
        bikeGang[2] = new Bicycle("Walmart", 0, -2);
        bikeGang[3] = new Bicycle("Mcdonalds", 40, 5);

        for (int i = 0; i < bikeGang.length; i++){
            System.out.println(bikeGang[i]);
        }
    }
}